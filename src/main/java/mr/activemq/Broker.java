package mr.activemq;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Broker {

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("mr/activemq/broker.xml");
        System.out.println("waiting....");
        System.in.read();
        context.close();
    }
}
