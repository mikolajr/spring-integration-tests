package mr.activemq;

import org.apache.activemq.util.TimeUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.util.Date;

@Component
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);

    protected static final String MESSAGE_COUNT = "messageCount";
    public static final String HASH_CODE = "hashCode";

    private JmsTemplate template = null;
    private int messageCount = 100;

    public void generateMessages() throws JMSException {
        long start = System.currentTimeMillis();

        for (int i = 0; i < messageCount; i++) {
            final int index = i;
            final String text = "Message number is " + i + ".";

            template.send(new MessageCreator() {
                public Message createMessage(Session session) throws JMSException {
                    TextMessage message = session.createTextMessage(text);
                    message.setIntProperty(MESSAGE_COUNT, index);

                    logger.info("Sending message: " + text);

                    return message;
                }
            });

            template.send(new MessageCreator() {
                @Override
                public Message createMessage(Session session) throws JMSException {
                    Pack pack = new Pack();
                    pack.setId(RandomUtils.nextInt());
                    pack.setName(RandomStringUtils.randomAlphanumeric(20));
                    pack.setBlob(RandomStringUtils.random(1000).getBytes());
                    pack.setDate(new Date());

                    ObjectMessage objectMessage = session.createObjectMessage();
                    objectMessage.setObject(pack);
                    objectMessage.setIntProperty(HASH_CODE, pack.hashCode());

                    logger.info("Sending object message: " + text);

                    return objectMessage;
                }
            });
        }

        System.out.println(TimeUtils.printDuration(System.currentTimeMillis() - start));
    }

    public void setTemplate(JmsTemplate template) {
        this.template = template;
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("mr/activemq/producer.xml");
        Producer producer = context.getBean("producer", Producer.class);
        System.out.println("waiting....");
        while (true) {
            producer.generateMessages();
            if (System.in.read() == -1) break;
        }
        context.close();
    }
}
