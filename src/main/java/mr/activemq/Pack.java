package mr.activemq;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public class Pack implements Serializable {

    private Date date;
    private Integer id;
    private String name;
    private byte[] blob;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pack pack = (Pack) o;

        if (!Arrays.equals(blob, pack.blob)) return false;
        if (!date.equals(pack.date)) return false;
        if (!id.equals(pack.id)) return false;
        if (!name.equals(pack.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + Arrays.hashCode(blob);
        return result;
    }

    @Override
    public String toString() {
        return "Pack{ date: " + date + "}\n";
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getBlob() {
        return blob;
    }

    public void setBlob(byte[] blob) {
        this.blob = blob;
    }
}
