package mr.activemq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class JaxbConsumer implements MessageListener {

    private static final Logger logger = LoggerFactory.getLogger(JaxbConsumer.class);

    private AtomicInteger counter = null;
    private MessageConverter messageConverter;

    public void onMessage(Message message) {
        try {
            counter.incrementAndGet();

            if (message instanceof TextMessage) {
                PackXml packXml = (PackXml) messageConverter.fromMessage(message);
                System.out.println(String.format("%s %s %s", Thread.currentThread().getName(), this.toString(), packXml));
            } else if (message instanceof ObjectMessage) {
                ObjectMessage om = (ObjectMessage) message;
                Serializable object = om.getObject();
                System.out.println(String.format("%s %s %s", Thread.currentThread().getName(), this.toString(), object));
                boolean eq = object.hashCode() == om.getIntProperty(Producer.HASH_CODE);
                if (!eq) {
                    System.out.printf("EQUALS failed for " + object);
                }
            }
            System.out.println("counter " + counter);
        } catch (JMSException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void setCounter(AtomicInteger counter) {
        this.counter = counter;
    }

    public void setMessageConverter(MessageConverter messageConverter) {
        this.messageConverter = messageConverter;
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("mr/activemq/jaxb_consumer.xml");
        context.getBean("consumer", JaxbConsumer.class);
        System.out.println("waiting....");
        System.in.read();
        context.close();
    }
}
