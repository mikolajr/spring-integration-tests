package mr.activemq.failover;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class PrepareDataToSend {

    public static void main(String[] args) throws IOException {
        cleanDirs("target/incoming-data", "target/file-data", "activemq-data", "activemq-data2");
        File rootDir = new File("target/file-data");
        FileUtils.deleteQuietly(rootDir);
        rootDir.mkdirs();
        for(int i=0; i<1000; i++) {
            String token = String.valueOf(i);
            FileUtils.write(new File(rootDir, token), token);
        }
    }

    private static void cleanDirs(String... dirs) {
        for (String dir : dirs) {
            FileUtils.deleteQuietly(new File(dir));
        }
    }
}
