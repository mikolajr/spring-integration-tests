package mr.activemq.failover;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StandaloneServer {

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:mr/activemq/failover/standalone-server.xml");
        System.out.println("Standalone ActiveMQ is running. Hit <Enter> to stop.");
        System.in.read();
        context.close();
    }
}
