package mr.activemq.failover;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

public class Producer {

    private JmsTemplate template;

    public void generateMessages() throws JMSException {
        int index = 0;

        while (true) {
            final String text = String.format("Message number is %d.", (++index));
            template.send(new MessageCreator() {
                public Message createMessage(Session session) throws JMSException {
                    TextMessage message = session.createTextMessage(text);
                    System.out.println("Sending: " + text);
                    return message;
                }
            });
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                break;
            }
        }
    }

    public void setTemplate(JmsTemplate template) {
        this.template = template;
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:mr/activemq/failover/producer.xml");
        Producer producer = context.getBean(Producer.class);
        System.out.println("Producer is running. Hit Ctrl-C to stop.");
        while (true) {
            producer.generateMessages();
            if (System.in.read() == -1) {
                break;
            }
        }
        context.close();
    }
}
