package mr.activemq.failover;

import org.apache.commons.io.FileUtils;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.File;
import java.io.IOException;

public class Consumer implements MessageListener {

    public void onMessage(Message message) {
        final File rootDir = new File("target/incoming-data");
        rootDir.mkdirs();

        try {
            if (message instanceof TextMessage) {
                TextMessage tm = (TextMessage)message;
                String msg = tm.getText();
                System.out.println(String.format("%s %s", Thread.currentThread().getName(), msg));
                String filename = String.format("%s-%d-%d", msg, Thread.currentThread().getId(), System.currentTimeMillis());
                try {
                    FileUtils.write(new File(rootDir, filename), msg);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Receiver message of type " + message.getClass().getName());
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:mr/activemq/failover/consumer.xml");
        context.getBean(Consumer.class);
        System.out.println("Consumer is running. Hit <Enter> to stop.");
        System.in.read();
        context.close();
    }
}
