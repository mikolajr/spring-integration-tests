package mr.activemq.failover;

import org.apache.commons.io.FileUtils;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.io.File;
import java.io.IOException;

public class FailoverProducer {

    private JmsTemplate template;

    public void generateMessages() throws JMSException {
        final File rootDir = new File("target/file-data");
        rootDir.mkdirs();

        for (File file : rootDir.listFiles()) {
            try {
                System.out.println("Reading file " + file);

                final String token = FileUtils.readFileToString(file);
                template.send(new MessageCreator() {
                    public Message createMessage(Session session) throws JMSException {
                        return session.createTextMessage(token);
                    }
                });
                file.delete();

                Thread.sleep(100);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                break;
            }
        }
    }

    public void setTemplate(JmsTemplate template) {
        this.template = template;
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:mr/activemq/failover/failover-producer.xml");
        FailoverProducer producer = context.getBean(FailoverProducer.class);
        System.out.println("Producer is running. Hit Ctrl-C to stop.");
        producer.generateMessages();
        context.close();
    }
}
