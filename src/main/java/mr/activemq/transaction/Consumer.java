package mr.activemq.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public class Consumer implements MessageListener {

    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);
    private int index = 0;

    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                TextMessage tm = (TextMessage) message;
                String msg = tm.getText();

//                if ((++index % 5) == 0) {
//                    throw new RuntimeException(msg);
//                }

                System.out.println("*" + ++index + " ***************** " + msg);
            }
        } catch (JMSException e) {
            logger.error(e.getMessage());
        }
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("mr/activemq/transaction/consumer.xml");
        context.getBean("consumer", Consumer.class);
        System.out.println("waiting....");
        System.in.read();
        context.close();
    }
}
