package mr.activemq.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

@Component
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);

    private JmsTemplate template = null;
    private static final int MESSAGE_COUNT = 100;
    private int index = 0;

    public void generateMessages() throws JMSException {
        for (int i = 0; i < MESSAGE_COUNT; i++) {
            final String text = String.format("Message number is %d.", (++index));
            template.send(new MessageCreator() {
                public Message createMessage(Session session) throws JMSException {
                    TextMessage message = session.createTextMessage(text);
                    logger.info("Sending: " + text);
                    return message;
                }
            });
        }
    }

    public void setTemplate(JmsTemplate template) {
        this.template = template;
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("mr/activemq/producer.xml");
        Producer producer = new Producer();
        producer.setTemplate((JmsTemplate) context.getBean("jmsProducerTemplate"));
        System.out.println("waiting....");
        while (true) {
            producer.generateMessages();
            if (System.in.read() == -1) break;
        }
        context.close();
    }
}
