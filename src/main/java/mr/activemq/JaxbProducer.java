package mr.activemq;

import org.apache.activemq.util.TimeUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Date;

@Component
public class JaxbProducer {

    private static final Logger logger = LoggerFactory.getLogger(JaxbProducer.class);

    protected static final String MESSAGE_COUNT = "messageCount";
    public static final String HASH_CODE = "hashCode";

    private JmsTemplate template = null;
    private int messageCount = 100;

    public void generateMessages() throws JMSException {
        long start = System.currentTimeMillis();

        for (int i = 0; i < messageCount; i++) {
            final int index = i;
            final String text = "Message number is " + i + ".";


            final PackXml pack = new PackXml();
            pack.setId(RandomUtils.nextInt());
            pack.setName(RandomStringUtils.randomAlphanumeric(20));
            pack.setBlob(RandomStringUtils.random(1000).getBytes());
            pack.setDate(new Date());
            logger.info("Sending object message: " + text);
            template.convertAndSend(pack, new MessagePostProcessor() {
                @Override
                public Message postProcessMessage(Message message) throws JMSException {
                    message.setIntProperty(HASH_CODE, pack.hashCode());
                    return message;
                }
            });
        }

        System.out.println(TimeUtils.printDuration(System.currentTimeMillis() - start));
    }

    public void setTemplate(JmsTemplate template) {
        this.template = template;
    }

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("mr/activemq/jaxb_producer.xml");
        JaxbProducer producer = context.getBean("producer", JaxbProducer.class);
        System.out.println("waiting....");
        while (true) {
            producer.generateMessages();
            if (System.in.read() == -1) break;
        }
        context.close();
    }
}
