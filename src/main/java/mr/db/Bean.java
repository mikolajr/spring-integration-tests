package mr.db;

import java.io.Serializable;
import java.util.Date;

public class Bean implements Serializable {
    private int id;
    private Date date;
    private String msg;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return String.format("[%s] %s", date, msg);
    }
}
