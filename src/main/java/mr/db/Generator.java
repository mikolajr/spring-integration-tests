package mr.db;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.Message;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

@ManagedResource
public class Generator implements MessageSource {

    private final static Log LOG = LogFactory.getLog(Generator.class);

    private AtomicInteger counter = new AtomicInteger(0);

    @Override
    public Message receive() {
        int id = counter.incrementAndGet();
        Bean bean = new Bean();
        bean.setDate(new Date());
        bean.setMsg(String.format("Message [%d]", id));
        bean.setId(id);

        LOG.fatal("Generator produced bean: " + bean);
        return MessageBuilder.withPayload(bean).build();
    }

    @ManagedAttribute
    public int getCounter() {
        return counter.get();
    }
}
