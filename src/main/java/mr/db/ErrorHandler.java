package mr.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.Message;
import org.springframework.integration.annotation.ServiceActivator;

public class ErrorHandler {

    private final static Log LOG = LogFactory.getLog(ErrorHandler.class);

    @ServiceActivator
    public void handleMessage(Message message) {
        String s = "*** ErrorHandler\n" +
                message.getHeaders() + "\n" +
                message.getPayload().getClass().getName() + "\n" +
                message.getPayload();
        LOG.error(s);
    }
}
