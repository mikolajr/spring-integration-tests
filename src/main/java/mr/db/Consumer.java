package mr.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@ManagedResource
public class Consumer {

    private final static Log LOG = LogFactory.getLog(Consumer.class);

    private AtomicBoolean shouldFail = new AtomicBoolean();
    private AtomicInteger consumed = new AtomicInteger();

    @ServiceActivator
    public void handleMessage(Bean bean) {
        LOG.fatal("Consumer consuming bean: " + bean);

        if (shouldFail.get()) {
            System.out.println("Consumer failing operation for " + bean);
            throw new RuntimeException("Failing for " + bean);
        }

        consumed.incrementAndGet();
    }

    @ManagedAttribute
    public void setShouldFail(boolean shouldFail) {
        this.shouldFail.set(shouldFail);
    }

    @ManagedAttribute
    public boolean getShouldFail() {
        return shouldFail.get();
    }

    @ManagedAttribute
    public int getConsumed() {
        return consumed.get();
    }
}
