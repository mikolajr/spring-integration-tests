package mr.db;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.Message;
import org.springframework.integration.annotation.Transformer;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@ManagedResource
public class Worker extends JdbcDaoSupport /*implements WorkerI*/ {

    private final static Log LOG = LogFactory.getLog(Worker.class);

    private AtomicInteger badId = new AtomicInteger(-1);
    private AtomicBoolean shouldFail = new AtomicBoolean();

    @Transformer
    public Message<Bean> transform(Message<Bean> message) throws WorkerException {
        Bean bean = message.getPayload();

        if (bean.getId() == badId.get()) {
            throw new WorkerException("BadId " + bean);
        }

        LOG.debug("Worker consumes Bean and stores in database" + bean);

        getJdbcTemplate().update("insert into dane (czas, dane) values (now(), ?)", bean.toString());

        if (shouldFail.get()) {
            LOG.warn("Worker failing operation for " + message);
            throw new RuntimeException("Failing for " + message);
        }

        return message;
    }

    @ManagedAttribute
    public void setShouldFail(boolean shouldFail) {
        this.shouldFail.set(shouldFail);
    }

    @ManagedAttribute
    public boolean getShouldFail() {
        return shouldFail.get();
    }

    @ManagedAttribute
    public int getBadId() {
        return badId.get();
    }

    @ManagedAttribute
    public void setBadId(int badId) {
        this.badId.set(badId);
    }

    public class WorkerException extends Exception {

        public WorkerException(String s) {
            super(s);
        }
    }
}
